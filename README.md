Wait until the gif opens (the project was not deployed due to server failure)

# Getting Started with Create React App---

1. `npm i`

## Available Scripts

In the project directory, you can run:

2.`npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
