import { BankOutlined } from "@ant-design/icons/lib/icons";
import React, { Component } from "react";

class Home extends Component {
  changeSelectMenu() {
    return localStorage.setItem("current", "/");
  }
  componentDidMount() {
    this.changeSelectMenu();
  }
  render() {
    return (
      <div className="container mt-25 home">
        <span>
          <BankOutlined className="bankIcon" />
        </span>
        <h1 className="green m-100">
          <b>
            Добро пожаловать в систему!!! <br />
          </b>
        </h1>
      </div>
    );
  }
}

export default Home;
