import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Select } from "antd";
import Form from "antd/lib/form/Form";
import Search from "antd/lib/input/Search";
import Modal from "antd/lib/modal/Modal";
import Text from "antd/lib/typography/Text";
import React, { Component } from "react";
import Highlighter from "react-highlight-words";
import { prossesData } from "../../server/data"; // baza o'rniga
import {
  processSearchByDate,
  processSearchByStatus,
  processStart,
  processStartOld,
  processStop,
} from "../../server/admin/api";
import { signOut } from "../../server/auth/authRequest";
import { today } from "../helpers/helper";

export default class Prosses extends Component {
  state = {
    process: [],
    filialCode: "",
    loading: false,
    isModalVisible: false,
    loadingStart: false,
    loadingStop: false,
    modalTitle: "",
    status: "",
  };

  startProcess = () => {
    this.setState({
      loading: true,
    });
  };

  // data fetching functions
  searchByDate = (value) => {
    this.setState({
      loading: true,
    });
    processSearchByDate(value)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            process: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        this.setState({
          loading: false,
        });
        if (value !== today) alert(err.response?.data.error_note || err);
      });

    // this.setState({
    //   process: prossesData.filter(function (p) {
    //     return p.data_begin === value || p.date_end === value;
    //   }),
    //   loading: false,
    // });
  };

  searchByStatus = () => {
    console.log(this.state.status);
    this.setState({
      loading: true,
    });
    processSearchByStatus(this.state.status)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            process: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        if (err.response?.status === 401) return signOut();
        this.setState({
          loading: false,
        });
        alert(err.response?.data.error_note || err);
      });
    // this.setState({
    //   process: prossesData.filter(function (p) {
    //     return p.status === value;
    //   }),
    //   loading: false,
    // });
  };

  // submit functions
  submitOkStart = () => {
    this.setState({ loadingStart: true });
    const { filialCode, modalTitle } = this.state;
    if (filialCode && modalTitle)
      modalTitle === "Запуск процесс"
        ? processStart(filialCode)
            .then((res) => {
              if (res) {
                alert(res.data.data.result_msg);
                this.setState({
                  isModalVisible: false,
                  loadingStart: false,
                  modalTitle: "",
                });
              }
            })
            .catch((err) => {
              if (err.response?.status === 401) return signOut();
              this.setState({ loadingStart: false });
              alert(err.response?.data.error_note || err);
            })
        : processStartOld(filialCode)
            .then((res) => {
              if (res) {
                alert(res.data.data.result_msg);
                this.setState({
                  isModalVisible: false,
                  loadingStart: false,
                  modalTitle: "",
                });
              }
            })
            .catch((err) => {
              if (err.response?.status === 401) return signOut();
              this.setState({ loadingStart: false });
              alert(err.response?.data.error_note || err);
            });
  };

  // handle functions
  handleCancelStart = () => {
    this.setState({
      isModalVisible: false,
      filialCode: "",
      loadingStart: false,
      modalTitle: "",
    });
  };

  handleCancelStop = () => {
    this.setState({ loading: true });
    processStop()
      .then((res) => {
        if (res) {
          alert(res.data.data.result_msg);
          this.setState({
            isModalVisible: false,
            loadingStop: false,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.setState({ loading: false });
      });
  };

  // fillter block
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            className="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "gold" : "#fff" }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  componentDidMount() {
    this.searchByDate(today);
  }

  render() {
    const prossesColumn = [
      // {
      //   title: "ID",
      //   dataIndex: "id",
      //   key: "id",
      //   ...this.getColumnSearchProps("id"),
      // },
      {
        title: "Дата начало",
        dataIndex: "date_begin",
        key: "data_begin",
        ...this.getColumnSearchProps("data_begin"),
      },
      {
        title: "Дата конец",
        dataIndex: "date_end",
        key: "date_end",
        ...this.getColumnSearchProps("date_end"),
      },
      {
        title: "Кол-во ощибок",
        dataIndex: "error_count",
        key: "error_count",
        ...this.getColumnSearchProps("error_count"),
      },
      {
        title: "Кол-во факт",
        dataIndex: "fact_count",
        key: "fact_count",
        ...this.getColumnSearchProps("fact_count"),
      },
      {
        title: "Статус",
        dataIndex: "status",
        key: "status",
        ...this.getColumnSearchProps("status"),
      },
      {
        title: "Колл-во успешных",
        dataIndex: "success_count",
        key: "success_count",

        ...this.getColumnSearchProps("success_count"),
      },
    ];
    return (
      <div className="container">
        <Text>
          <h2 className="mt-25">Процессы</h2>
        </Text>
        <Space direction="gorizontal" className="mb-25">
          <Input.Group>
            <Select
              style={{ width: "220px" }}
              placeholder="Поиск по статусу"
              onChange={(value) => {
                this.setState({ status: value });
              }}
            >
              <Select.Option value="created">Создан</Select.Option>
              <Select.Option value="in_processing">В прогрессе</Select.Option>
              <Select.Option value="processed">Выполнен</Select.Option>
              <Select.Option value="prepared">Подготовлен</Select.Option>
              <Select.Option value="error">Ошибка</Select.Option>
            </Select>
          </Input.Group>
          <Button
            loading={this.state.loading}
            type="primary"
            disabled={this.state.status === ""}
            onClick={this.searchByStatus}
          >
            <SearchOutlined />
          </Button>
          <Search
            allowClear
            enterButton
            defaultValue={today}
            type={"date"}
            onSearch={this.searchByDate}
          />
          <Button
            type="primary"
            className="primary"
            onClick={() =>
              this.setState({
                isModalVisible: true,
                modalTitle: "Запуск процесс",
              })
            }
          >
            Запуск
          </Button>
          <Button
            type="primary"
            className="primary"
            onClick={() =>
              this.setState({
                isModalVisible: true,
                modalTitle: "Запуск для просрочных",
              })
            }
          >
            Запуск для просрочных
          </Button>
          <Button
            loading={this.state.loadingStop}
            type="danger"
            onClick={this.handleCancelStop}
          >
            Остановить
          </Button>
        </Space>

        <Modal
          visible={this.state.isModalVisible}
          title={this.state.modalTitle}
          onCancel={this.handleCancelStart}
          footer={false}
        >
          <Form
            onFinish={this.submitOkStart}
            onFinishFailed={() => {
              alert("Произошла ощибка");
            }}
          >
            <Input
              required={true}
              allowClear
              placeholder="filial code..."
              value={this.state.filialCode}
              onChange={(e) => this.setState({ filialCode: e.target.value })}
            ></Input>
            <Button
              type="primary"
              htmlType="submit"
              className="mt-25 mr-25 primary"
              loading={this.state.loadingStart}
            >
              Запуск
            </Button>
            <Button
              type="danger"
              className="mt-25"
              onClick={() => {
                this.setState({ filialCode: "" });
                this.handleCancelStart();
              }}
              disabled={this.state.loadingStart}
            >
              Отменить
            </Button>
          </Form>
        </Modal>

        <Table
          loading={this.state.loading}
          dataSource={this.state.process}
          columns={prossesColumn}
          size="small"
          scroll={{ x: "100%" }}
          pagination={{ pageSize: 10 }}
          className="components-table-demo-nested"
          rowKey={"id"}
          bordered
        />
      </div>
    );
  }
}
