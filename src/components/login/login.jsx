import React, { Component } from "react";
import { Button, Card, Form, Input } from "antd";
import { signin } from "../../server/auth/authRequest";
import { getUser } from "../../utilits";

export default class Login extends Component {
  state = {
    isUser: false,
    loading: false,
  };

  onLoading = () => {
    this.setState({
      loading: true,
    });
  };

  render() {
    const onFinish = (values) => {
      this.onLoading();
      signin(values.username, values.password)
        .then((res) => {
          if (res && res.data.status == "Success") {
            localStorage.setItem("user", JSON.stringify(res.data.data));
            localStorage.setItem("current", "");
            this.setState({
              isUser: true,
              loading: false,
            });
          } else {
            alert("Неверный логин или парол");
          }
        })
        .catch((err) => {
          alert(err.response?.data.error_note || err);
          this.setState({
            loading: false,
          });
        });
      // if (values) {
      //   localStorage.setItem(
      //     "user",
      //     JSON.stringify({
      //       token: "12345",
      //       role:
      //         values.username === "admin"
      //           ? "admin"
      //           : "superadmin"
      //           ? "superAdmin"
      //           : "",
      //     })
      //   );
      //   localStorage.setItem("current", "");
      //   this.setState({
      //     isUser: true,
      //     loading: false,
      //   });
      // } else {
      //   alert("Xato!");
      //   this.setState({
      //     loading: false,
      //   });
      // }
    };

    return (
      <div className="loginPage">
        <Card title="СИСТЕМА АВТОПЛАТЕЖ ДЛЯ КРЕДИТОВ" className="loginCard">
          {getUser() && getUser().token && window.location.replace("/")}
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: "Пожалуйста, введите ваше имя пользователя!",
                },
              ]}
            >
              <Input allowClear placeholder="Имя пользователя" />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Пожалуйста, введите ваш пароль!",
                },
              ]}
            >
              <Input.Password allowClear placeholder="Пароль" />
            </Form.Item>

            <Form.Item>
              <Button
                loading={this.state.loading}
                type="primary"
                htmlType="submit"
                className="primary"
              >
                Вход в систему
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}
