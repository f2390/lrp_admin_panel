import {
  EditOutlined,
  PauseCircleOutlined,
  PlayCircleOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Button, Input, Space, Table } from "antd";
import Form from "antd/lib/form/Form";
import Modal from "antd/lib/modal/Modal";
import Text from "antd/lib/typography/Text";
import React, { Component } from "react";
import Highlighter from "react-highlight-words";
import {
  getScheduler,
  startSchedulerAPI,
  stopSchedulerAPI,
  updateSchedulerAPI,
} from "../../server/admin/api";
import { signOut } from "../../server/auth/authRequest";
import { schedulerData } from "../../server/data";

export default class Schedulers extends Component {
  state = {
    schedulers: [],
    loading: false,
    isModalVisible: false,
    expression: "",
    schedulerId: "",
  };

  onLoading = (loading_status) => {
    this.setState({
      loading: loading_status,
    });
  };

  getSchedulers = () => {
    this.onLoading(true);
    getScheduler()
      .then((res) => {
        this.setState({
          schedulers: res.data.data,
          loading: false,
        });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
    // this.setState({
    //   schedulers: schedulerData,
    //   loading: false,
    // });
  };

  updateScheduler = () => {
    this.onLoading(true);
    const { expression, schedulerId } = this.state;
    if (schedulerId && expression) {
      updateSchedulerAPI(schedulerId, expression)
        .then((res) => {
          alert(res.data.data.result_msg);
          this.getSchedulers();
          this.setState({
            expression: "",
            schedulerId: "",
            isModalVisible: false,
          });
        })
        .catch((err) => {
          alert(err.response?.data.error_note || err);
          this.setState({
            expression: "",
            schedulerId: "",
            loading: false,
          });
        });
    }
  };

  startScheduler = (id) => {
    this.onLoading(true);
    startSchedulerAPI(id)
      .then((res) => {
        alert(res.data.data.result_msg);
        this.getSchedulers();
      })
      .catch((err) => {
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  stopScheduler = (id) => {
    this.onLoading(true);
    stopSchedulerAPI(id)
      .then((res) => {
        alert(res.data.data.result_msg);
        this.getSchedulers();
      })
      .catch((err) => {
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  handleCancelEdit = () => {
    this.setState({
      isModalVisible: false,
      expression: "",
      loading: false,
    });
  };
  // fillter
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            className="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "gold" : "#fff" }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  componentDidMount() {
    this.getSchedulers();
  }
  render() {
    const { loading, schedulers } = this.state;
    const schedulersColumn = [
      /*{
        title: "ID",
        dataIndex: "id",
        key: "id",
        ...this.getColumnSearchProps("id"),
      },*/
      {
        title: "Код",
        dataIndex: "code",
        key: "code",
        ...this.getColumnSearchProps("code"),
      },
      {
        title: "Название",
        dataIndex: "name",
        key: "name",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "Выражение",
        dataIndex: "expression",
        key: "expression",
        ...this.getColumnSearchProps("expression"),
      },

      {
        title: "Функция",
        dataIndex: "exec_func",
        key: "exec_func",
        ...this.getColumnSearchProps("exec_func"),
      },
      {
        title: "Ид процесса",
        dataIndex: "job_id",
        key: "job_id",
        width: "130px",
        ...this.getColumnSearchProps("job_id"),
      },
      {
        title: "Статус",
        dataIndex: "status",
        key: "status",
        ...this.getColumnSearchProps("status"),
      },
      {
        title: "Дата изменения",
        dataIndex: "updated_at",
        key: "updated_at",
        width: "150px",
        ...this.getColumnSearchProps("updated_at"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        width: "150px",
        ...this.getColumnSearchProps("created_at"),
      },
      {
        //fixed: "right",
        width: "180px",
        title: "Действия",
        key: "actions",
        render: (scheduler) => (
          <div key={scheduler.id}>
            <Button
              type="primary"
              className="mr-25"
              onClick={() =>
                this.setState({
                  isModalVisible: true,
                  schedulerId: scheduler.id,
                  expression: scheduler.expression,
                })
              }
            >
              <EditOutlined />
            </Button>
            <Button
              type="primary primary"
              className="mr-25"
              onClick={() => this.startScheduler(scheduler.id)}
            >
              <PlayCircleOutlined />
            </Button>
            <Button
              type="danger"
              onClick={() => this.stopScheduler(scheduler.id)}
            >
              <PauseCircleOutlined />
            </Button>
          </div>
        ),
      },
    ];
    return (
      <div className="container">
        <Text>
          <h2 className="mt-25 mb-25">Планировщики</h2>
        </Text>
        <Modal
          visible={this.state.isModalVisible}
          title={"Изменить планировщик"}
          onCancel={this.handleCancelEdit}
          footer={false}
        >
          <Form
            onFinish={this.updateScheduler}
            onFinishFailed={() => {
              alert("Произощла ощибка");
            }}
          >
            <Input
              required={true}
              allowClear
              placeholder="expression..."
              value={this.state.expression}
              onChange={(e) => this.setState({ expression: e.target.value })}
            ></Input>
            <Button
              type="primary"
              htmlType="submit"
              className="mt-25 mr-25 primary"
              loading={this.state.loading}
            >
              Изменить
            </Button>
            <Button
              type="danger"
              className="mt-25"
              onClick={this.handleCancelEdit}
              disabled={this.state.loading}
            >
              Отменить
            </Button>
          </Form>
        </Modal>
        <Table
          rowKey="id"
          loading={loading}
          dataSource={schedulers}
          columns={schedulersColumn}
          scroll={{ x: "100%" }}
          size="small"
          pagination={{ pageSize: 10 }}
          className="components-table-demo-nested"
          bordered
        />
      </div>
    );
  }
}
