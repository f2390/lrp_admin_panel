import React, { Component } from "react";
import { Button, Menu } from "antd";
import { Link } from "react-router-dom";
import {
  LogoutOutlined,
  FileTextOutlined,
  BarChartOutlined,
  HomeOutlined,
  SettingOutlined,
  ScheduleOutlined,
  TransactionOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout } from "antd";
import { signOut } from "../../server/auth/authRequest";
import { getUser } from "../../utilits";

class Header extends Component {
  state = {
    current: localStorage.getItem("current") || "/",
  };

  handleClick = (e) => {
    if (e.key !== "logout") {
      localStorage.setItem("current", e.key);
      this.setState({ current: e.key });
    }
  };

  render() {
    const { current } = this.state;
    const { Header } = Layout;
    return (
      <Header className="header">
        {getUser() && (
          <Menu
            theme="dark"
            className="header-container"
            onClick={this.handleClick}
            selectedKeys={[current]}
            mode="horizontal"
            defaultSelectedKeys={"/"}
          >
            <Menu.Item icon={<HomeOutlined />} key="/">
              <Link to="/">
                <b>Главный</b>
              </Link>
            </Menu.Item>
            <Menu.Item key="contracts" icon={<FileTextOutlined />}>
              <Link to="/contracts">Договоры</Link>
            </Menu.Item>
            {(getUser().role === "admin" || getUser().role === "superAdmin") && (
              <React.Fragment>
                <Menu.Item key="process" icon={<BarChartOutlined />}>
                  <Link to="/process">Процессы</Link>
                </Menu.Item>
                <Menu.Item icon={<ScheduleOutlined />} key="Schedulers">
                  <Link to="/schedulers">Планировщики</Link>
                </Menu.Item>
                <Menu.Item icon={<TransactionOutlined />} key="transacts">
                  <Link to="/transacts">Транзакции</Link>
                </Menu.Item>
                {getUser().role === "superAdmin" && (
                  <Menu.Item icon={<UserOutlined />} key="users">
                    <Link to="/users">Пользователи</Link>
                  </Menu.Item>
                )}

                <Menu.Item icon={<SettingOutlined />} key="params">
                  <Link to="/params">Параметры</Link>
                </Menu.Item>
              </React.Fragment>
            )}

            <Menu.Item
              key="logout"
              style={{
                backgroundColor: "#001529",
              }}
            >
              <Button
                onClick={signOut}
                type={"primary"}
                className="primary"
                icon={<LogoutOutlined />}
              >
                Выход
              </Button>
            </Menu.Item>
          </Menu>
        )}
      </Header>
    );
  }
}

export default Header;
