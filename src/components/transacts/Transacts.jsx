import { FileTextOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Input, Select, Space, Table } from "antd";
import Search from "antd/lib/input/Search";
import Text from "antd/lib/typography/Text";
import React, { Component } from "react";
import Highlighter from "react-highlight-words";
import { transactData } from "../../server/data"; // baza o'rniga
import {
  getProtocols,
  getTransactByLoanId,
  searchTransactByDate,
  searchTransactByState,
  sendTransactAction,
} from "../../server/admin/api";
import { signOut } from "../../server/auth/authRequest";
import { today } from "../helpers/helper";
import Modal from "antd/lib/modal/Modal";

export default class Transacts extends Component {
  state = {
    transacts: [],
    loading: false,
    today: today,
    action: "",
    selectedRowKeys: [],
    status: "",
    beginTime: today,
    endTime: today,
    openModalForGetProtocols: false,
    openModalForGetTransactsByLoanId: false,
    transactsByLoanID: [],
    protocols: [],
  };

  startProcess = () => {
    this.setState({
      loading: true,
    });
  };

  // data fetching
  searchByDate = (beginTime, endTime) => {
    this.setState({
      loading: true,
    });
    searchTransactByDate(beginTime, endTime)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            transacts: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        beginTime !== this.state.today &&
          endTime !== this.state.today &&
          alert(err.response?.data.error_note || err);
        this.setState({
          loading: false,
        });
      });
    // this.setState({
    //   transacts: transactData,
    //   loading: false,
    // });
  };

  searchByStatus = () => {
    this.setState({
      loading: true,
    });
    searchTransactByState(this.state.status)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            transacts: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        this.setState({
          loading: false,
        });
        alert(err.response?.data.error_note || err);
      });
    // this.setState({
    //   transacts: transactData.filter(function (t) {
    //     return t.state === value;
    //   }),
    //   loading: false,
    // });
  };

  fetchProtocols = (id) => {
    this.setState({ loading: true });
    getProtocols(id)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            protocols: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();

        alert(err.response?.data.error_note || err);
        this.setState({
          loading: false,
        });
      });
  };

  // submit function
  submitTransactAction = () => {
    this.setState({ loading: true });
    const { action, selectedRowKeys } = this.state;
    sendTransactAction(action, selectedRowKeys)
      .then((res) => {
        alert(res.data.data.result_msg);
        this.setState({ loading: false });
        //this.searchByDate(this.state.today);
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.setState({ loading: false });
      });
  };
  submitSearchByLoanId = (value) => {
    this.setState({ openModalForGetTransactsByLoanId: true, loading: true });
    getTransactByLoanId(value)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          if (res.data.data.length === 0) {
            alert("Не найден");
            this.setState({
              loading: false,
              transactsByLoanID: [],
              openModalForGetTransactsByLoanId: false,
            });
          } else
            this.setState({
              transactsByLoanID: res.data.data,
              loading: false,
            });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.setState({
          loading: false,
          transactsByLoanID: [],
          openModalForGetTransactsByLoanId: false,
        });
      });
    // this.setState({
    //   openModalForGetTransactsByLoanId: true,
    //   transactsByLoanID: transactData,
    // });
  };

  // fillter
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            className="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "gold" : "#fff" }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  // lifecycle
  componentDidMount() {
    //this.setState({ transacts: transactData });
    this.searchByDate(this.state.today, this.state.today);
  }

  render() {
    const {
      selectedRowKeys,
      action,
      loading,
      status,
      beginTime,
      endTime,
      openModalForGetProtocols,
      protocols,
      openModalForGetTransactsByLoanId,
      transactsByLoanID,
    } = this.state;

    // columns
    const transactColumns = [
      /*{
          title: "ID",
          dataIndex: "id",
          key: "id",
          ...this.getColumnSearchProps("id"),
        },*/
      {
        title: "Карта",
        dataIndex: "card_id",
        key: "card_id",
        ...this.getColumnSearchProps("card_id"),
      },
      {
        title: "Сумма",
        dataIndex: "amount",
        key: "amount",
        ...this.getColumnSearchProps("amount"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        width: "150px",
        ...this.getColumnSearchProps("created_at"),
      },
      {
        title: "Дата изменения",
        dataIndex: "updated_at",
        key: "updated_at",
        width: "150px",
        ...this.getColumnSearchProps("updated_at"),
      },
      {
        title: "Ид из Пц",
        dataIndex: "processing_id",
        key: "processing_id",
        ...this.getColumnSearchProps("processing_id"),
      },
      {
        title: "РРН",
        dataIndex: "ps_ref_num",
        key: "ps_ref_num",
        ...this.getColumnSearchProps("ps_ref_num"),
      },
      {
        title: "Ид терминала",
        dataIndex: "terminal_id",
        key: "terminal_id",
        ...this.getColumnSearchProps("terminal_id"),
      },
      {
        title: "Статус",
        dataIndex: "state",
        key: "state",
        ...this.getColumnSearchProps("state"),
      },
      // {
      //   title: "commission_amount",
      //   dataIndex: "commission_amount",
      //   key: "commission_amount",
      //   ...this.getColumnSearchProps("commission_amount"),
      // },
      {
        title: "Код ощибки",
        dataIndex: "error_code",
        key: "error_code",
        width: "150px",
        ...this.getColumnSearchProps("error_code"),
      },
      {
        title: "Текст ощибки",
        dataIndex: "error_message",
        key: "error_message",
        width: "150px",
        ...this.getColumnSearchProps("error_message"),
      },
      {
        title: "Внешный ид",
        dataIndex: "external_id",
        key: "external_id",
        ...this.getColumnSearchProps("external_id"),
      },
      {
        //fixed: "right",
        title: "Действия",
        key: "actions",
        render: (tr) => (
          <div key={tr.id}>
            <Button
              icon={<FileTextOutlined />}
              type="primary"
              onClick={() => {
                this.setState({
                  openModalForGetProtocols: true,
                });
                this.fetchProtocols(tr.id);
              }}
            >
              Протокол
            </Button>
          </div>
        ),
      },
    ];

    const protocolColumn = [
      {
        title: "Действия",
        dataIndex: "action",
        key: "action",
        ...this.getColumnSearchProps("action"),
      },
      {
        title: "Пользователь",
        dataIndex: "user",
        key: "user",
        ...this.getColumnSearchProps("user"),
      },
      {
        title: "Сообщение",
        dataIndex: "message",
        key: "message",
        ...this.getColumnSearchProps("message"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        ...this.getColumnSearchProps("created_at"),
      },
    ];
    return (
      <div className="container">
        <Text>
          <h2 className="mt-25 mb-25">Транзакции</h2>
        </Text>
        <Space className="mb-25 mr-25">
          <Input.Group>
            <Select
              style={{ width: "260px" }}
              placeholder="Действия"
              onChange={(value) => {
                this.setState({ action: value });
              }}
            >
              <Select.Option value="GET_PS_STATUS">
                Получить статус транзакции из ПЦ
              </Select.Option>
              <Select.Option value="CANCEL_AUTH">
                Возврать авторизации
              </Select.Option>
              <Select.Option value="REPEAT_TO_IABS">
                Повторно отправить на проводку
              </Select.Option>
              <Select.Option value="RETURN_PAYMENT">
                Отменить транзакции
              </Select.Option>
              <Select.Option value="TO_PROCESSED">
                Изменить на выполнен
              </Select.Option>
              <Select.Option value="TO_REJECT">
                Изменить на забракован
              </Select.Option>
            </Select>
            <Button
              className="primary"
              loading={loading}
              type="primary"
              disabled={action === "" || selectedRowKeys.length === 0}
              onClick={this.submitTransactAction}
            >
              Выполнить
            </Button>
          </Input.Group>

          <Input.Group>
            <Select
              style={{ width: "200px" }}
              placeholder="Поиск по статусу"
              onChange={(value) => {
                this.setState({ status: value });
              }}
            >
              <Select.Option value="created">Создан</Select.Option>
              <Select.Option value="in_processing">В прогрессе</Select.Option>
              <Select.Option value="processed">Выполнен</Select.Option>
              <Select.Option value="rejected">Забракован</Select.Option>
              <Select.Option value="critical">Критичный</Select.Option>
              <Select.Option value="in_manual">
                В ручной обработке
              </Select.Option>
            </Select>{" "}
            <Button
              loading={loading}
              type="primary"
              disabled={status === ""}
              onClick={this.searchByStatus}
            >
              <SearchOutlined />
            </Button>
          </Input.Group>
          <Search
            placeholder="Введите ид кредита"
            onSearch={this.submitSearchByLoanId}
            enterButton
            disabled={loading}
          />
        </Space>

        <Space className="mb-25">
          <Text>Начало:</Text>
          <Input
            title="BeginTime"
            loading={loading}
            allowClear
            enterButton={false}
            type={"date"}
            defaultValue={this.state.today}
            onChange={(e) => {
              this.setState({ beginTime: e.target.value });
            }}
          />
          <Text>Конец:</Text>
          <Input
            title="EndTime"
            loading={loading}
            allowClear
            enterButton={false}
            type={"date"}
            defaultValue={this.state.today}
            onChange={(e) => {
              this.setState({ endTime: e.target.value });
            }}
          />
          <Button
            disabled={!beginTime && !endTime}
            type="primary"
            onClick={() => this.searchByDate(beginTime, endTime)}
          >
            <SearchOutlined />
          </Button>
        </Space>

        {/*-------------------modals-----------------------*/}
        {/* main modal */}
        <Modal
          width="100%"
          visible={openModalForGetProtocols}
          onCancel={() => this.setState({ openModalForGetProtocols: false })}
          footer={false}
          title={`Протоколы`}
        >
          <Table
            loading={this.state.loading}
            dataSource={protocols}
            columns={protocolColumn}
            scroll={{ x: "100%" }}
            size="small"
            pagination={{ pageSize: 10 }}
            className="components-table-demo-nested"
            rowKey={"id"}
            bordered
          />
        </Modal>

        {/* transacts by loan id modal */}
        <Modal
          visible={openModalForGetTransactsByLoanId}
          loading={loading}
          width="100%"
          footer={false}
          title={"Транзакции по кредита"}
          onOk={() =>
            this.setState({ openModalForGetTransactsByLoanId: false })
          }
          onCancel={() =>
            this.setState({ openModalForGetTransactsByLoanId: false })
          }
        >
          <Table
            loading={this.state.loading}
            dataSource={transactsByLoanID}
            columns={transactColumns}
            scroll={{ x: "100%" }}
            size="small"
            pagination={{ pageSize: 10 }}
            className="components-table-demo-nested"
            rowKey={"id"}
            bordered
          />
        </Modal>

        {/* main table */}
        <Table
          loading={this.state.loading}
          dataSource={this.state.transacts}
          columns={transactColumns}
          scroll={{ x: "100%" }}
          size="small"
          pagination={{ pageSize: 10 }}
          className="components-table-demo-nested"
          rowKey={"id"}
          bordered
          rowSelection={{
            ...selectedRowKeys,
            onChange: (s) => {
              this.setState({ selectedRowKeys: s });
            },
          }}
        />
      </div>
    );
  }
}
