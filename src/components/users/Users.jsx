import {
  SearchOutlined,
  UserAddOutlined,
  EditOutlined,
  EyeTwoTone,
  EyeInvisibleOutlined,
} from "@ant-design/icons/lib/icons";
import { Button, Form, Input, Modal, Select, Space, Table } from "antd";
import React, { Component } from "react";
import Highlighter from "react-highlight-words";
import {
  addUser,
  getUser,
  updateUserPassword,
  updateUserState,
} from "../../server/admin/api";
import { signOut } from "../../server/auth/authRequest";
import { usersData } from "../../server/data";
import { onFinishFailed } from "../helpers/helper";

export default class Users extends Component {
  state = {
    listUser: [],
    mainLoading: false,
    openAddUserModal: false,
    openEditStatusModal: false,
    openEditPasswordModal: false,
    editUserId: "",
    editUserStatus: "",
  };

  // helpers
  onLoading(loading) {
    this.setState({ mainLoading: loading });
  }

  // fectch data methods
  fetchUsers() {
    this.onLoading(true);
    getUser()
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            listUser: res.data.data,
            mainloading: false,
          });
        }
        this.onLoading(false);
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
    //this.setState({ listUser: usersData });
  }

  // submit functions
  submitAddUser = (values) => {
    this.onLoading(true);
    addUser(
      values.local_code,
      values.login,
      values.name,
      values.password,
      values.role
    )
      .then((res) => {
        if (res) {
          alert(res.data.data.result_msg);
          this.fetchUsers();
        }
        this.setState({ openAddUserModal: false });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  submitEditStatusUser = (values) => {
    console.log(values);
    this.onLoading(true);
    updateUserState(this.state.editUserId, values.state)
      .then((res) => {
        if (res) {
          alert(res.data.data.result_msg);
          this.fetchUsers();
        }
        this.setState({ openEditStatusModal: false });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  submitEditPasswordUser = (values) => {
    this.onLoading(true);
    updateUserPassword(this.state.editUserId, values.password)
      .then((res) => {
        if (res) {
          alert(res.data.data.result_msg);
          this.fetchUsers();
        }
        this.setState({ openEditPasswordModal: false });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };
  // fillter
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            className="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "gold" : "#fff" }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  // lifecycles
  componentDidMount() {
    this.fetchUsers();
  }
  render() {
    const {
      mainLoading,
      listUser,
      openAddUserModal,
      openEditStatusModal,
      openEditPasswordModal,
      editUserStatus,
    } = this.state;

    const usersColumns = [
      // {
      //   title: "ID",
      //   dataIndex: "id",
      //   key: "id",
      //   ...this.getColumnSearchProps("id"),
      // },
      {
        title: "Имя",
        dataIndex: "name",
        key: "name",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "Логин",
        dataIndex: "login",
        key: "login",
        ...this.getColumnSearchProps("login"),
      },
      {
        title: "Локалный код",
        dataIndex: "local_code",
        key: "local_code",
        width: "150px",
        ...this.getColumnSearchProps("local_code"),
      },
      {
        title: "Роль",
        dataIndex: "role_code",
        key: "role_code",
        width: "150px",
        ...this.getColumnSearchProps("role_code"),
      },
      {
        title: "Статус",
        dataIndex: "status",
        key: "status",
        ...this.getColumnSearchProps("status"),
      },
      {
        title: "Изменить",
        key: "action",
        width: "230px",
        render: (user) => (
          <div key={user.id}>
            <Button
              type="primary"
              className="mr-25"
              onClick={() =>
                this.setState({
                  openEditStatusModal: true,
                  editUserId: user.id,
                  editUserStatus: user.status,
                })
              }
              icon={<EditOutlined />}
            >
              Статус
            </Button>
            <Button
              type="primary"
              className="primary"
              onClick={() =>
                this.setState({
                  openEditPasswordModal: true,
                  editUserId: user.id,
                  editUserPassword: user.password,
                })
              }
              icon={<EditOutlined />}
            >
              Пароль
            </Button>
          </div>
        ),
      },
    ];
    return (
      <div className="container">
        <h2 className="mt-25 mb-25">Пользователи</h2>

        <Space className="mb-25">
          <Button
            className="primary"
            type="primary"
            icon={<UserAddOutlined />}
            loading={mainLoading}
            onClick={() => this.setState({ openAddUserModal: true })}
          >
            Добавить ползователь
          </Button>
        </Space>

        <Table
          loading={mainLoading}
          dataSource={listUser}
          columns={usersColumns}
          scroll={{ x: "100%" }}
          size="small"
          pagination={{ pageSize: 10 }}
          className="components-table-demo-nested"
          rowKey={"id"}
          bordered
        />
        {/*--------------------Modals------------------------ */}
        <Modal
          title={"Добавить ползователь"}
          visible={openAddUserModal}
          onCancel={() => this.setState({ openAddUserModal: false })}
          footer={false}
        >
          <Form
            name="addUser"
            initialValues={{
              remember: true,
            }}
            onFinish={this.submitAddUser}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Имя"
              name="name"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Пароль"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Логин"
              name="login"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Локалный код"
              name="local_code"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Роль"
              name="role"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item>
              <Button
                loading={mainLoading}
                className="mr-25 primary"
                type="primary"
                htmlType="submit"
              >
                Добавить
              </Button>
              <Button
                onClick={() => this.setState({ openAddUserModal: false })}
                type="danger"
              >
                Отменить
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/*edit status modal*/}
        <Modal
          title={"Изменить статус"}
          visible={openEditStatusModal}
          onCancel={() => this.setState({ openEditStatusModal: false })}
          footer={false}
        >
          <Form
            name="editStatusUser"
            initialValues={{
              remember: true,
            }}
            onFinish={this.submitEditStatusUser}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Статус"
              name="state"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
              initialValue={editUserStatus}
            >
              <Select>
                <Select.Option value={1}>Актив</Select.Option>
                <Select.Option value={0}>Пассив</Select.Option>
              </Select>
            </Form.Item>

            <Form.Item>
              <Button
                loading={mainLoading}
                className="mr-25 primary"
                type="primary"
                htmlType="submit"
              >
                Добавить
              </Button>
              <Button
                onClick={() => this.setState({ openEditStatusModal: false })}
                type="danger"
              >
                Отменить
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/*edit password modal*/}
        <Modal
          title={"Изменить парол"}
          visible={openEditPasswordModal}
          onCancel={() => this.setState({ openEditPasswordModal: false })}
          footer={false}
        >
          <Form
            name="editPasswordUser"
            initialValues={{
              remember: true,
            }}
            onFinish={this.submitEditPasswordUser}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Парол"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
            >
              <Input.Password
                placeholder="Парол"
                allowClear
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
              />
            </Form.Item>

            <Form.Item>
              <Button
                loading={mainLoading}
                className="mr-25 primary"
                type="primary"
                htmlType="submit"
              >
                Добавить
              </Button>
              <Button
                onClick={() => this.setState({ openEditPasswordModal: false })}
                type="danger"
              >
                Отменить
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}
