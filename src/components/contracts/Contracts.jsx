import React, { Component } from "react";
import {
  cardData,
  contractData,
  graphicData,
  transactData,
} from "../../server/data";

import { Button, Input, Modal, Space, Table, Form, Select } from "antd";
import {
  CreditCardOutlined,
  LineChartOutlined,
  FileTextOutlined,
  SearchOutlined,
  TransactionOutlined,
  DeleteOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
  EditOutlined,
} from "@ant-design/icons/lib/icons";
import Highlighter from "react-highlight-words";
import {
  connectByDoc,
  contactAction,
  deleteCard,
  deleteContract,
  editCard,
  getCard,
  getContract,
  getContractByCardNumber,
  getContractByLoanId,
  getGraphic,
  getProtocolsByContractId,
  getTransact,
  reloadGraphic,
  sendConfirmCode,
  sendUserCardData,
} from "../../server/admin/api";
import Text from "antd/lib/typography/Text";
//import { onFinishFailed, openNotificationSuccess } from "../helpers/helper";

import Card from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";
import { signOut } from "../../server/auth/authRequest";
const { Search } = Input;

class Contracts extends Component {
  state = {
    contracts: [],
    contractId: "",
    cards: [],
    graphics: [],
    transacts: [],
    modalTitle: "",
    isTableModalVisible: false,
    groups: [],
    loading: true,
    loadingDelete: false,
    deleteContractId: null,
    deleteCardId: null,
    openModalForDeleteContract: false,
    openModalForConnectCard: false,
    openModalForConnectCardByDoc: false,
    number: "",
    expiry: "",
    issuer: "",
    focused: "",
    inputLoanID: "",
    operationId: "",
    confirmCode: "",
    pinfl: "",
    filialCode: "",
    secretCode: "",
    selectedRowKeys: [],
    action: "",
    contractsByLoanId: [],
    openModalContractsByLoanId: false,
    openModalForEditCard: false,
    openModalContractsByCardNumber: false,
    openModalProtocolsByContractId: false,
    contractsByCardNumber: [],
    protocolsByContractId: [],
    editCardId: "",
    editCardOrder: "",
  };

  editCardOrdRef = React.createRef();
  // modal controllers
  showModal = () => {
    this.setState({
      isTableModalVisible: true,
    });
  };

  handleOk = () => {
    this.setState({
      isTableModalVisible: false,
      deleteContractId: null,
      modalTitle: "",
      openModalForDeleteContract: false,
    });
  };

  handleCancel = () => {
    this.setState({
      isTableModalVisible: false,
      deleteContractId: null,
      deleteCardId: null,
      modalTitle: "",
      secretCode: "",
      loading: false,
      operationId: "",
      confirmCode: "",
      inputLoanID: "",
      pinfl: "",
      number: "",
      expiry: "",
    });
    setTimeout(() => {
      this.setState({
        openModalForDeleteContract: false,
        openModalForConnectCard: false,
        openModalForConnectCardByDoc: false,
      });
    }, 300);
  };

  onLoading = (loading_status) => {
    this.setState({
      loading: loading_status,
    });
  };

  // data fetching and CRUD
  getContracts = () => {
    this.onLoading(true);
    getContract()
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            contracts: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });

    // this.setState({
    //   contracts: contractData,
    //   loading: false,
    // });
  };

  getCards = (id) => {
    this.onLoading(true);
    getCard(id)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            cards: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });

    // this.setState({
    //   cards: cardData.filter(function (c) {
    //     return c.contract_id === id;
    //   }),
    //   loading: false,
    // });
  };

  getGraphics = (id) => {
    this.onLoading(true);
    getGraphic(id)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            graphics: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
    // this.setState({
    //   graphics: graphicData.filter(function (g) {
    //     return g.contract_id === id;
    //   }),
    //   loading: false,
    // });
  };

  getTransacts = (id) => {
    this.onLoading(true);
    getTransact(id)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            transacts: res.data.data,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        if (err.response?.status === 401) return signOut();
        this.onLoading(false);
      });
    // this.setState({
    //   transacts: transactData.filter(function (t) {
    //     return t.contract_id === id;
    //   }),
    //   loading: false,
    // });
  };

  reloadGraphics = (id) => {
    this.onLoading(true);
    reloadGraphic(id)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          this.setState({
            responseMessage: res.data.data.result_msg,
            loading: false,
          });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
    // alert("bajarildi!");
    // this.setState({
    //   loading: false,
    // });
  };

  deleteCardHandle = () => {
    this.setState({ loadingDelete: true });
    const { deleteCardId, secretCode } = this.state;
    if (deleteCardId && secretCode) {
      deleteCard(deleteCardId, secretCode)
        .then((res) => {
          alert(res.data.data.result_msg);
          this.setState({ loadingDelete: false });
        })
        .catch((err) => {
          if (err.response?.status === 401) return signOut();
          alert(err.response?.data.error_note || err);
          this.setState({ loadingDelete: false });
        });
    } else {
      alert("Card Id or Secret code does not exists!");
    }
  };

  // fillter
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            className="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "gold" : "#fff" }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  // for user card
  handleCallback = ({ issuer }, isValid) => {
    if (isValid) {
      this.setState({ issuer });
    }
  };

  handleInputFocus = ({ target }) => {
    this.setState({
      focused: target.name,
    });
  };

  handleInputChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };
  // {card_expire: "0924", card_number: "8600041521862004", loan_id: ""}

  // submit functions
  submitDeleteContract = () => {
    this.onLoading(true);

    let id = this.state.deleteContractId;
    let secretCode = this.state.secretCode;

    if (id) {
      deleteContract(id, secretCode)
        .then((res) => {
          if (res && res.data) {
            this.handleOk();
            this.getContracts();
            this.onLoading(false);
            alert(res.data.data.result_msg);
          } else {
            this.handleCancel();
            alert("Error");
          }
        })
        .catch((err) => {
          if (err.response?.status === 401) return signOut();
          alert(err.response?.data.error_note || err);
          this.onLoading(false);
        });
    }
    // alert("salom");
    // this.onLoading(false);
  };

  submitConnectUserCard = (value) => {
    this.onLoading(true);
    const { inputLoanID, number, expiry } = this.state;
    sendUserCardData(inputLoanID, number, expiry, value.personType)
      .then((res) => {
        if (res && res.data.data)
          this.setState({
            operationId: res.data.data.operation_id,
            filialCode: res.data.data.filial_code,
            isTableModalVisible: false,
            loading: false,
            inputLoanID: "",
            number: "",
            expiry: "",
          });
        this.setState({
          isTableModalVisible: false,
          loading: false,
          inputLoanID: "",
          number: "",
          expiry: "",
        });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
    // this.setState({
    //   isTableModalVisible: false,
    //   operationId: "12345",
    //   loading: false,
    //   inputLoanID: "",
    //   number: "",
    //   expiry: "",
    // });
  };

  submitConnectUserCardFinish = () => {
    this.onLoading(true);
    const { operationId, confirmCode } = this.state;
    sendConfirmCode(operationId, confirmCode)
      .then((res) => {
        if (res && res.data) alert(res.data.data.result_msg);
        this.handleCancel();
        this.getContracts();
        this.setState({
          inputLoanID: "",
          number: "",
          expiry: "",
          operationId: "",
          confirmCode: "",
        });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
        this.setState({
          inputLoanID: "",
          number: "",
          expiry: "",
        });
      });
  };

  submitConnectUserCardByDoc = (value) => {
    this.onLoading(true);
    const { inputLoanID, pinfl } = this.state;
    connectByDoc(inputLoanID, pinfl, value.personType)
      .then((res) => {
        if (res && res.data) alert(res.data.data.result_msg);
        this.getContracts();
        this.setState({
          inputLoanID: "",
          pinfl: "",
        });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  submitContractAction = () => {
    this.onLoading(true);
    contactAction(this.state.selectedRowKeys, this.state.action)
      .then((res) => {
        if (res && res.data) alert(res.data.data.result_msg);
        this.setState({ loading: false });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  submitSearchByLoanId = (value) => {
    this.setState({ openModalContractsByLoanId: true, loading: true });
    getContractByLoanId(value)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          if (res.data.data.length === 0) {
            alert("Не найден");
            this.setState({
              loading: false,
              contractsByLoanId: [],
              openModalContractsByLoanId: false,
            });
          } else
            this.setState({
              contractsByLoanId: res.data.data,
              loading: false,
            });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.setState({
          loading: false,
          contractsByLoanId: [],
          openModalContractsByLoanId: false,
        });
      });

    // this.setState({
    //   contractsByLoanId: contractData,
    //   loading: false,
    // });
  };

  submitSearchByCardNumber = (value) => {
    this.setState({
      openModalContractsByCardNumber: true,
      loading: true,
    });
    getContractByCardNumber(value)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          if (res.data.data.length === 0) {
            alert("Не найден");
            this.setState({
              loading: false,
              contractsByCardNumber: [],
              openModalContractsByCardNumber: false,
            });
          } else
            this.setState({
              contractsByCardNumber: res.data.data,
              loading: false,
            });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.setState({
          contractsByCardNumber: [],
          openModalContractsByCardNumber: false,
        });
        this.onLoading(false);
      });
  };

  submitEditCard = (value) => {
    this.onLoading(true);
    editCard(this.state.editCardId, value.ord)
      .then((res) => {
        if (res && res.data) alert(res.data.data.result_msg);
        this.getCards(this.state.contractId);
        this.setState({ openModalForEditCard: false, loading: false });
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.onLoading(false);
      });
  };

  submitGetProtocolsByContractId = (value) => {
    this.setState({
      openModalProtocolsByContractId: true,
      loading: true,
    });
    getProtocolsByContractId(value)
      .then((res) => {
        if (res && Array.isArray(res.data.data)) {
          if (res.data.data.length === 0) {
            alert("Не найден");
            this.setState({
              loading: false,
              protocolsByContractId: [],
              openModalProtocolsByContractId: false,
            });
          } else
            this.setState({
              protocolsByContractId: res.data.data,
              loading: false,
            });
        }
      })
      .catch((err) => {
        if (err.response?.status === 401) return signOut();
        alert(err.response?.data.error_note || err);
        this.setState({
          protocolsByContractId: [],
          openModalProtocolsByContractId: false,
        });
        this.onLoading(false);
      });
  };

  // lifecycles
  componentDidMount() {
    this.getContracts();
  }

  render() {
    const {
      contracts,
      contractsByLoanId,
      isTableModalVisible,
      loading,
      cards,
      graphics,
      transacts,
      modalTitle,
      deleteCardId,
      openModalForDeleteContract,
      openModalForConnectCard,
      openModalForConnectCardByDoc,
      number,
      expiry,
      focused,
      issuer,
      operationId,
      filialCode,
      selectedRowKeys,
      action,
      openModalContractsByLoanId,
      openModalContractsByCardNumber,
      openModalProtocolsByContractId,
      protocolsByContractId,
      contractsByCardNumber,
      openModalForEditCard,
      editCardOrder,
    } = this.state;

    // column schemas
    const contractColumns = [
      /*{
        title: "ID",
        dataIndex: "id",
        key: "id",
        ...this.getColumnSearchProps("id"),
      },*/
      {
        title: "Код филиала",
        dataIndex: "filial_code",
        key: "filial_code",
        width: "130px",
        ...this.getColumnSearchProps("filial_code"),
      },
      {
        title: "Ид кредита",
        dataIndex: "loan_id",
        key: "loan_id",

        ...this.getColumnSearchProps("loan_id"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        width: "140px",
        ...this.getColumnSearchProps("created_at"),
      },
      /*{
        title: "repayment date",
        dataIndex: "repayment_date",
        key: "repayment_date",
        ...this.getColumnSearchProps("repayment_date"),
      },*/
      {
        title: "Статус",
        dataIndex: "state",
        key: "state",
        ...this.getColumnSearchProps("state"),
      },
      {
        title: "Дата конец договора",
        dataIndex: "close_date",
        key: "close_date",
        width: "190px",
        ...this.getColumnSearchProps("close_date"),
      },
      {
        title: "Дата изменения",
        dataIndex: "updated_at",
        key: "updated_at",
        width: "150px",
        ...this.getColumnSearchProps("updated_at"),
      },

      /*{
        title: "loan client id",
        dataIndex: "loan_client_id",
        key: "loan_client_id",
        ...this.getColumnSearchProps("loan_client_id"),
      },*/

      {
        //fixed: "right",
        title: "Действия",
        key: "actions",
        width: "305px",
        render: (contract) => (
          <div key={contract.id}>
            <Button
              title={"Карты"}
              // type="primary"
              className="mr-25  primary"
              onClick={() => {
                this.showModal();
                this.setState({
                  modalTitle: "Карты",
                  contractId: contract.id,
                });
                this.getCards(contract.id);
              }}
            >
              <CreditCardOutlined />
            </Button>
            <Button
              title={"Графики"}
              type="primary"
              className="mr-25"
              onClick={() => {
                this.showModal();
                this.setState({ modalTitle: "Графики" });
                this.getGraphics(contract.id);
              }}
            >
              <LineChartOutlined />
            </Button>
            <Button
              title={"Транзакции"}
              type="primary"
              className="mr-25"
              onClick={() => {
                this.showModal();
                this.setState({ modalTitle: "Транзакции" });
                this.getTransacts(contract.id);
              }}
            >
              <TransactionOutlined />
            </Button>
            <Button
              title="Протоколы"
              type="primary"
              className=" mr-25"
              onClick={() => {
                this.submitGetProtocolsByContractId(contract.id);
              }}
            >
              <FileTextOutlined />
            </Button>
            <Button
              title={"Открепить кредита"}
              type="danger"
              onClick={() => {
                this.showModal();
                this.setState({
                  modalTitle: `Открепить кредита`,
                  openModalForDeleteContract: true,
                  deleteContractId: contract.id,
                });
              }}
            >
              <DeleteOutlined />
            </Button>
          </div>
        ),
      },
    ];

    const cardColumns = [
      /*{
        title: "ID",
        dataIndex: "id",
        key: "id",
        ...this.getColumnSearchProps("id"),
      },*/
      {
        title: "Ид карты",
        dataIndex: "card_id",
        key: "card_id",
        ...this.getColumnSearchProps("card_id"),
      },
      {
        title: "Номер карты",
        dataIndex: "card_number",
        key: "card_number",
        ...this.getColumnSearchProps("card_number"),
      },
      {
        title: "Срок действия",
        dataIndex: "date_expire",
        key: "date_expire",
        ...this.getColumnSearchProps("date_expire"),
      },
      {
        title: "Сорт",
        dataIndex: "ord",
        key: "ord",
        ...this.getColumnSearchProps("ord"),
      },
      {
        title: "Статус",
        dataIndex: "state",
        key: "state",
        ...this.getColumnSearchProps("state"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        ...this.getColumnSearchProps("created_at"),
      },
      {
        title: "Дата измениния",
        dataIndex: "updated_at",
        key: "updated_at",
        ...this.getColumnSearchProps("updated_at"),
      },
      {
        title: "Владелец карты",
        dataIndex: "person_type",
        key: "person_type",
        ...this.getColumnSearchProps("person_type"),
      },
      {
        //fixed: "right",
        title: "Действия",
        key: "actions",
        render: (card) => (
          <div key={card.id}>
            <Button
              type="primary"
              className="mr-25"
              onClick={() => {
                this.setState({
                  editCardId: card.id,
                  editCardOrder: card.ord,
                  openModalForEditCard: true,
                });
                if (this.editCardOrdRef.current) {
                  this.editCardOrdRef.current.setFieldsValue({
                    ord: card.ord,
                  });
                }
              }}
            >
              <EditOutlined />
            </Button>
            <Button
              type="danger"
              onClick={() => {
                this.setState({ deleteCardId: card.id });
              }}
              disabled={deleteCardId}
            >
              <DeleteOutlined />
            </Button>
          </div>
        ),
      },
    ];

    const graphicColumns = [
      /*{
        title: "ID",
        dataIndex: "id",
        key: "id",
        ...this.getColumnSearchProps("id"),
      },*/
      {
        title: "Дата графика",
        dataIndex: "repayment_date",
        key: "repayment_date",
        ...this.getColumnSearchProps("repayment_date"),
      },
      {
        title: "Статус",
        dataIndex: "state",
        key: "state",
        ...this.getColumnSearchProps("state"),
      },
      {
        title: "Interest onterm",
        dataIndex: "interest_onterm",
        key: "interest_onterm",
        ...this.getColumnSearchProps("interest_onterm"),
      },
      {
        title: "Рекомендуемая сумма",
        dataIndex: "recommended_amount",
        key: "recommended_amount",
        ...this.getColumnSearchProps("recommended_amount"),
      },
      {
        title: "Остаток",
        dataIndex: "amount",
        key: "amount",
        ...this.getColumnSearchProps("amount"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        ...this.getColumnSearchProps("created_at"),
      },
      {
        title: "Дата изменения",
        dataIndex: "updated_at",
        key: "updated_at",
        ...this.getColumnSearchProps("updated_at"),
      },
    ];

    const transactColumns = [
      /*{
        title: "ID",
        dataIndex: "id",
        key: "id",
        ...this.getColumnSearchProps("id"),
      },*/
      {
        title: "Карта",
        dataIndex: "card_id",
        key: "card_id",
        ...this.getColumnSearchProps("card_id"),
      },
      {
        title: "Сумма",
        dataIndex: "amount",
        key: "amount",
        ...this.getColumnSearchProps("amount"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        ...this.getColumnSearchProps("created_at"),
      },
      {
        title: "Дата изменения",
        dataIndex: "updated_at",
        key: "updated_at",
        ...this.getColumnSearchProps("updated_at"),
      },
      {
        title: "Ид из Пц",
        dataIndex: "processing_id",
        key: "processing_id",
        ...this.getColumnSearchProps("processing_id"),
      },
      {
        title: "РРН",
        dataIndex: "ps_ref_num",
        key: "ps_ref_num",
        ...this.getColumnSearchProps("ps_ref_num"),
      },
      {
        title: "Ид терминала",
        dataIndex: "terminal_id",
        key: "terminal_id",
        ...this.getColumnSearchProps("terminal_id"),
      },
      {
        title: "Статус",
        dataIndex: "state",
        key: "state",
        ...this.getColumnSearchProps("state"),
      },
      /*{
        title: "commission_amount",
        dataIndex: "commission_amount",
        key: "commission_amount",
        ...this.getColumnSearchProps("commission_amount"),
      },*/
      {
        title: "Код ощибки",
        dataIndex: "error_code",
        key: "error_code",
        ...this.getColumnSearchProps("error_code"),
      },
      {
        title: "Текст ощибки",
        dataIndex: "error_message",
        key: "error_message",
        ...this.getColumnSearchProps("error_message"),
      },
      {
        title: "Внешный ид",
        dataIndex: "external_id",
        key: "external_id",
        ...this.getColumnSearchProps("external_id"),
      },
    ];

    const protocolColumn = [
      {
        title: "Действия",
        dataIndex: "action",
        key: "action",
        ...this.getColumnSearchProps("action"),
      },
      {
        title: "Сообщение",
        dataIndex: "message",
        key: "message",
        ...this.getColumnSearchProps("message"),
      },
      {
        title: "Дата создания",
        dataIndex: "created_at",
        key: "created_at",
        ...this.getColumnSearchProps("created_at"),
      },
    ];

    return (
      <div className="container">
        <Text>
          <h2 className="mt-25">Договоры</h2>
        </Text>
        <Space direction="gorizontal" className="mb-25">
          <Button
            className="primary"
            type="primary"
            onClick={() => {
              this.showModal();
              this.setState({
                modalTitle: "Добавить карту",
                openModalForConnectCard: true,
              });
            }}
          >
            Добавить карту
          </Button>
          <Button
            className="primary"
            type="primary"
            onClick={() => {
              this.showModal();
              this.setState({
                modalTitle: "Добавить карты через ПИНФЛ",
                openModalForConnectCardByDoc: true,
              });
            }}
          >
            Добавить карты через ПИНФЛ
          </Button>
        </Space>
        <br />
        <Space className="mb-25 mr-25" direction="gorizontal">
          {/* contract actions */}
          <Input.Group>
            <Select
              style={{ width: "250px" }}
              placeholder="Действия"
              onChange={(value) => {
                this.setState({ action: value });
              }}
            >
              <Select.Option value="REPAYMENT">Погасить</Select.Option>
              <Select.Option value="OVERDUE">
                Погасить за просрочных
              </Select.Option>
              <Select.Option value="RESET_GRAPHIC_STATE">
                Обновить статус графика
              </Select.Option>
              <Select.Option value="RELOAD_GRAPHIC">
                Обновить график
              </Select.Option>
            </Select>
            <Button
              className="primary"
              loading={loading}
              type="primary"
              disabled={action === "" || selectedRowKeys.length === 0}
              onClick={this.submitContractAction}
            >
              Выполнить
            </Button>
          </Input.Group>

          {/* search loan id */}
          <Search
            placeholder="Введите ид кредита"
            onSearch={this.submitSearchByLoanId}
            enterButton
            disabled={loading}
          />

          {/* search by card number */}
          <Search
            placeholder="Введите номер карты"
            onSearch={this.submitSearchByCardNumber}
            enterButton
            disabled={loading}
          />
        </Space>

        {/* main modal */}
        <Modal
          title={modalTitle}
          visible={isTableModalVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={false}
          loading={loading}
          width={
            openModalForDeleteContract ||
            openModalForConnectCard ||
            openModalForConnectCardByDoc
              ? "500px"
              : "100%"
          }
        >
          {openModalForDeleteContract ? (
            <Form
              name="basic3"
              onFinish={this.submitDeleteContract}
              onFinishFailed={this.onFinishFailed}
            >
              <Form.Item
                label="Секретный код"
                name="secretCodeContract"
                rules={[
                  {
                    required: true,
                    message: "Please input secretCode!",
                  },
                ]}
                initialValue={""}
              >
                <Input.Password
                  placeholder="Secret code..."
                  required={true}
                  onChange={(e) =>
                    this.setState({ secretCode: e.target.value })
                  }
                  iconRender={(visible) =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                  allowClear
                />
              </Form.Item>

              <Form.Item>
                <Button
                  loading={loading}
                  className="mr-25"
                  type="danger"
                  htmlType="submit"
                >
                  Удалить
                </Button>
                <Button
                  onClick={this.handleCancel}
                  type="primary"
                  className="primary"
                >
                  Отменить
                </Button>
              </Form.Item>
            </Form>
          ) : openModalForConnectCard ? (
            <React.Fragment>
              <Form
                name="basic4"
                id="connectUserCardForm"
                onFinish={this.submitConnectUserCard}
                onFinishFailed={this.onFinishFailed}
              >
                <Form.Item>
                  <Card
                    number={number}
                    expiry={expiry}
                    focused={focused}
                    callback={this.handleCallback}
                    name="Банковская карта"
                    cvc="cvc"
                  />
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        marginTop: "15px",
                      }}
                    >
                      <Input
                        type="tel"
                        name="number"
                        value={number}
                        maxLength={16}
                        placeholder="Номер карты"
                        pattern="[\d| ]{16,22}"
                        required
                        onChange={this.handleInputChange}
                        onFocus={this.handleInputFocus}
                        className="mr-25"
                      />
                      <Input
                        type="tel"
                        name="expiry"
                        value={expiry}
                        maxLength={4}
                        placeholder="Срок карты"
                        pattern="\d\d\d\d"
                        required
                        onChange={this.handleInputChange}
                        onFocus={this.handleInputFocus}
                      />
                    </div>
                    <Input
                      name="inputLoanID"
                      placeholder="Идентификатор кредита"
                      required
                      value={this.state.inputLoanID}
                      onChange={this.handleInputChange}
                      onFocus={this.handleInputFocus}
                      className="mt-25"
                      style={{ width: "390px" }}
                    />
                    <Form.Item
                      label=""
                      placeholder="Владелец карты"
                      name="personType"
                      rules={[
                        {
                          required: true,
                          message: "Please input value!",
                        },
                      ]}
                      initialValue={0}
                    >
                      <Select className="mt-25" style={{ width: "390px" }}>
                        <Select.Option value={0}>Должник</Select.Option>
                        <Select.Option value={1}>Поручитель</Select.Option>
                        <Select.Option value={2}>Созаемщик</Select.Option>
                      </Select>
                    </Form.Item>
                  </div>

                  <input type="hidden" name="issuer" value={issuer} />
                </Form.Item>

                <Form.Item>
                  <Button
                    type="primary"
                    loading={loading}
                    className="mr-25 primary"
                    htmlType="submit"
                  >
                    Добавить
                  </Button>
                  <Button
                    htmlType="reset"
                    type="danger"
                    onClick={() => {
                      this.handleCancel();
                      this.setState({
                        inputLoanID: "",
                        number: "",
                        expiry: "",
                      });
                    }}
                  >
                    Отменить
                  </Button>
                </Form.Item>
              </Form>
            </React.Fragment>
          ) : openModalForConnectCardByDoc ? (
            <React.Fragment>
              <Form
                name="basic5"
                onFinish={this.submitConnectUserCardByDoc}
                onFinishFailed={this.onFinishFailed}
              >
                <Form.Item>
                  <Input
                    placeholder="Идентификатор кредита"
                    required
                    value={this.state.inputLoanID}
                    onChange={(e) =>
                      this.setState({ inputLoanID: e.target.value })
                    }
                  />

                  <Input
                    placeholder="Введите ПИНФЛ..."
                    required
                    value={this.state.pinfl}
                    className="mt-25"
                    onChange={(e) => this.setState({ pinfl: e.target.value })}
                  />
                </Form.Item>
                <Form.Item
                  label=""
                  placeholder="Владелец карты"
                  name="personType"
                  rules={[
                    {
                      required: true,
                      message: "Please input value!",
                    },
                  ]}
                  initialValue={0}
                >
                  <Select>
                    <Select.Option value={0}>Должник</Select.Option>
                    <Select.Option value={1}>Поручитель</Select.Option>
                    <Select.Option value={2}>Созаемщик</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item>
                  <Button
                    type="primary"
                    loading={loading}
                    className="mr-25 primary"
                    htmlType="submit"
                  >
                    Добавить
                  </Button>
                  <Button
                    type="danger"
                    onClick={() => {
                      this.setState({
                        inputLoanID: "",
                        pinfl: "",
                      });
                      this.handleCancel();
                    }}
                  >
                    Отменить
                  </Button>
                </Form.Item>
              </Form>
            </React.Fragment>
          ) : (
            <>
              <Table
                loading={loading}
                size="small"
                dataSource={
                  modalTitle === "Карты"
                    ? cards
                    : modalTitle === "Графики"
                    ? graphics
                    : modalTitle === "Транзакции"
                    ? transacts
                    : []
                }
                columns={
                  modalTitle === "Карты"
                    ? cardColumns
                    : modalTitle === "Графики"
                    ? graphicColumns
                    : modalTitle === "Транзакции"
                    ? transactColumns
                    : []
                }
                scroll={{ x: "100%" }}
                pagination={{ pageSize: 10 }}
                rowKey={"id"}
                bordered
              />

              <Button
                onClick={() => this.setState({ isTableModalVisible: false })}
                className="mr-25 primary"
                type="primary"
              >
                Закрыть
              </Button>
            </>
          )}
        </Modal>

        {/* ikkilamchi modallar */}
        <Modal
          title={`Открепить карту`}
          visible={deleteCardId}
          loading={loading}
          width={"500px"}
          onCancel={() =>
            this.setState({ deleteCardId: false, secretCode: "" })
          }
          footer={false}
        >
          <Form
            name="basic2"
            onFinish={() => this.deleteCardHandle()}
            onFinishFailed={() => {
              alert("Произошла ошибка");
            }}
          >
            <Form.Item
              label="Секретный код"
              name="secretCode"
              rules={[
                {
                  required: true,
                  message: "Please input secretCode!",
                },
              ]}
            >
              <Input.Password
                placeholder="Secret code..."
                required={true}
                onChange={(e) => this.setState({ secretCode: e.target.value })}
                iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                }
              />
            </Form.Item>
            <Button
              className="mr-25"
              type="danger"
              htmlType="submit"
              loading={this.state.loadingDelete}
            >
              Удалить
            </Button>
            <Button
              onClick={() =>
                this.setState({ deleteCardId: false, secretCode: "" })
              }
              className="primary"
            >
              Отменить
            </Button>
          </Form>
        </Modal>

        {/* connect card modals */}
        <Modal
          visible={operationId}
          loading={loading}
          title={"Введите код подтверждения"}
          footer={false}
          onCancel={() => this.setState({ operationId: "", confirmCode: "" })}
        >
          <Form
            name="basic10"
            onFinish={this.submitConnectUserCardFinish}
            onFinishFailed={this.onFinishFailed}
          >
            <Text type="success" className="mb-25">
              Код филиала: {filialCode}
            </Text>
            <Form.Item>
              <Input
                placeholder="Введите код подтверждения..."
                required
                allowClear
                className="mt-25"
                value={this.state.confirmCode}
                onChange={(e) => this.setState({ confirmCode: e.target.value })}
              />
            </Form.Item>
            <Form.Item>
              <Button
                className="mr-25 primary"
                type="primary"
                htmlType="submit"
                loading={loading}
              >
                Отправить
              </Button>
              <Button
                onClick={() =>
                  this.setState({ operationId: "", confirmCode: "" })
                }
                className="mr-25"
                type="danger"
              >
                Отменить
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* modal for get contracts by loan id */}
        <Modal
          visible={openModalContractsByLoanId}
          loading={loading}
          width="100%"
          footer={false}
          title={"Договоры по кредита"}
          onOk={() => this.setState({ openModalContractsByLoanId: false })}
          onCancel={() => this.setState({ openModalContractsByLoanId: false })}
        >
          <Table
            rowKey={"id"}
            size="small"
            loading={loading}
            dataSource={contractsByLoanId}
            columns={contractColumns}
            scroll={{ x: "100%" }}
            pagination={{ pageSize: 10 }}
            className="components-table-demo-nested"
            bordered
            width="auto"
          />
        </Modal>

        {/* modal for get contracts by card number */}
        <Modal
          visible={openModalContractsByCardNumber}
          loading={loading}
          width="100%"
          footer={false}
          title={"Договоры по номера карты"}
          onOk={() => this.setState({ openModalContractsByCardNumber: false })}
          onCancel={() =>
            this.setState({ openModalContractsByCardNumber: false })
          }
        >
          <Table
            rowKey={"id"}
            size="small"
            loading={loading}
            dataSource={contractsByCardNumber}
            columns={contractColumns}
            scroll={{ x: "100%" }}
            pagination={{ pageSize: 10 }}
            className="components-table-demo-nested"
            bordered
            width="auto"
          />
        </Modal>

        {/* modal for get protocols by contract id */}
        <Modal
          visible={openModalProtocolsByContractId}
          loading={loading}
          width="100%"
          footer={false}
          title={"Протоколы по ид контракты"}
          onOk={() => this.setState({ openModalProtocolsByContractId: false })}
          onCancel={() =>
            this.setState({ openModalProtocolsByContractId: false })
          }
        >
          <Table
            rowKey={"id"}
            size="small"
            loading={loading}
            dataSource={protocolsByContractId}
            columns={protocolColumn}
            scroll={{ x: "100%" }}
            pagination={{ pageSize: 10 }}
            className="components-table-demo-nested"
            bordered
            width="auto"
          />
        </Modal>

        {/* modal for edit card */}
        <Modal
          visible={openModalForEditCard}
          title={`Изменить сортировку`}
          footer={false}
          onCancel={() => {
            this.setState({
              openModalForEditCard: false,
              editCardOrder: "",
            });
          }}
        >
          <Form
            name="editCard"
            //initialValues={{ remember: false, ord: editCardOrder }}
            onFinish={this.submitEditCard}
            onFinishFailed={this.onFinishFailed}
            ref={this.editCardOrdRef}
            initialValues={{
              remember: true,
            }}
          >
            <Form.Item
              label="Сорт"
              name="ord"
              rules={[
                {
                  required: true,
                  message: "Please input value!",
                },
              ]}
              initialValue={editCardOrder}
            >
              <Input placeholder="Введите cорт карту..." required allowClear />
            </Form.Item>
            <Form.Item>
              <Button
                className="mr-25 primary"
                type="primary"
                htmlType="submit"
                loading={loading}
              >
                Сохранить
              </Button>
              <Button
                onClick={() => this.setState({ openModalForEditCard: false })}
                className="mr-25"
                type="danger"
              >
                Отменить
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        {/* main table */}
        <Table
          rowKey={"id"}
          size="small"
          loading={loading}
          dataSource={contracts}
          columns={contractColumns}
          scroll={{ x: "100%" }}
          pagination={{ pageSize: 10 }}
          className="components-table-demo-nested"
          bordered
          width="auto"
          rowSelection={{
            ...selectedRowKeys,
            onChange: (s) => {
              this.setState({ selectedRowKeys: s });
            },
          }}
        />
      </div>
    );
  }
}

export default Contracts;
