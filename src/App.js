import "antd/dist/antd.css";
import "./App.css";
import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import Contracts from "./components/contracts/Contracts";
import Home from "./components/home/home";
import Header from "./components/header/header";
import { Layout, Result } from "antd";
import Login from "./components/login/login";
import { getUser } from "./utilits";
import Prosses from "./components/prosses/Prosses";
import Params from "./components/params/Params";
import Schedulers from "./components/schedulers/Schedulers";
import Transacts from "./components/transacts/Transacts";
import Users from "./components/users/Users";

const App = () => {
  const [selectMenu, setSelectMenu] = useState("/");
  return (
    <Layout>
      <div className="app">
        <Router>
          {getUser() ? (
            <React.Fragment>
              <Header sM={selectMenu} />
              <Switch>
                <Route exact path="/">
                  <Home sSM={setSelectMenu} />
                </Route>
                <Route path="/login">
                  <Home />
                  <Redirect to="/" />
                </Route>
                <Route path="/contracts">
                  <Contracts sSM={setSelectMenu} />
                </Route>
                {(getUser().role === "admin" ||
                  getUser().role === "superAdmin") && (
                  <React.Fragment>
                    <Route path="/process">
                      <Prosses sSM={setSelectMenu} />
                    </Route>
                    <Route path="/params">
                      <Params sSM={setSelectMenu} />
                    </Route>
                    <Route path="/schedulers">
                      <Schedulers sSM={setSelectMenu} />
                    </Route>
                    <Route path="/transacts">
                      <Transacts sSM={setSelectMenu} />
                    </Route>
                    {getUser().role === "superAdmin" && (
                      <Route path="/users">
                        <Users sSM={setSelectMenu} />
                      </Route>
                    )}
                  </React.Fragment>
                )}

                <Route>
                  <Result
                    sSM={setSelectMenu}
                    status="404"
                    title="404"
                    subTitle="Sorry, the page you visited does not exist."
                  />
                  ,
                </Route>
              </Switch>
            </React.Fragment>
          ) : (
            <div>
              <Login />
              <Redirect to="login" />
            </div>
          )}
        </Router>
      </div>
    </Layout>
  );
};

export default App;
