import { host, httpRequest } from "../host";
import { deleteLocalStorage } from "../../utilits";
export let signin = (username, password) => {
  let config = {
    url: `${host}/api/v1/loan/login`,
    method: "post",
    data: {
      username: username,
      password: password,
    },
  };
  return httpRequest(config);
};

export let signOut = () => {
  deleteLocalStorage("user");
  window.location.reload();
};

// export let authMe = () => {
//   let config = {
//     url: `${host}/api/auth/me`,
//     method: "get",
//   };
//   return httpRequest(config);
// };
