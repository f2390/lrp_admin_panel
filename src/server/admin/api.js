import { host, httpRequest } from "../host";

// for contract page
export let getContract = () => {
  let config = {
    url: `${host}/api/v1/loan/contracts`,
    method: "get",
  };
  return httpRequest(config);
};

export let deleteContract = (id, secretCode) => {
  let config = {
    url: `${host}/api/v1/loan/contract/delete`,
    method: "delete",
    data: {
      contract_id: id,
      secret_code: secretCode,
    },
  };
  return httpRequest(config);
};

export let getCard = (contract_id) => {
  let config = {
    url: `${host}/api/v1/loan/cards/${contract_id}`,
    method: "get",
  };
  return httpRequest(config);
};

export let deleteCard = (id, secretCode) => {
  let config = {
    url: `${host}/api/v1/loan/contract/card/delete`,
    method: "delete",
    data: {
      card_id: id,
      secret_code: secretCode,
    },
  };
  return httpRequest(config);
};

export let getGraphic = (contract_id) => {
  let config = {
    url: `${host}/api/v1/loan/graphics/${contract_id}`,
    method: "get",
  };
  return httpRequest(config);
};

export let getTransact = (contract_id) => {
  let config = {
    url: `${host}/api/v1/loan/transacts/${contract_id}`,
    method: "get",
  };
  return httpRequest(config);
};

export let reloadGraphic = (loan_id) => {
  let config = {
    url: `${host}/api/v1/loan/reload-graphics`,
    method: "post",
    data: {
      loan_id: loan_id,
    },
  };
  return httpRequest(config);
};

export const sendUserCardData = (
  loanId,
  cardNumber,
  cardExpireDate,
  personType
) => {
  let config = {
    url: `${host}/api/v1/loan/add/check`,
    method: "post",
    data: {
      card_expire: cardExpireDate,
      card_number: cardNumber,
      loan_id: loanId,
      person_type: personType,
    },
  };
  return httpRequest(config);
};

export const sendConfirmCode = (reqAddLoanConfirm, operationId) => {
  let config = {
    url: `${host}/api/v1/loan/add/confirm`,
    method: "post",
    data: {
      confirm_code: operationId,
      operation_id: reqAddLoanConfirm,
    },
  };
  return httpRequest(config);
};

export const connectByDoc = (loanId, pinfl, personType) => {
  let config = {
    url: `${host}/api/v1/loan/add/card/by-pinfl`,
    method: "post",
    data: {
      loan_id: loanId,
      pinfl: pinfl,
      person_type: personType,
    },
  };
  return httpRequest(config);
};

export const getContractByLoanId = (loanId) => {
  let config = {
    url: `${host}/api/v1/loan/contracts/by-loan/${loanId}`,
    method: "get",
  };
  return httpRequest(config);
};

export const getContractByCardNumber = (card_number) => {
  let config = {
    url: `${host}/api/v1/loan/contracts/by-card/${card_number}`,
    method: "get",
  };
  return httpRequest(config);
};

export const contactAction = (id, action) => {
  let config = {
    url: `${host}/api/v1/loan/contracts/action`,
    method: "put",
    data: {
      action: action,
      id: id,
    },
  };
  return httpRequest(config);
};

export const editCard = (id, ord) => {
  let config = {
    url: `${host}/api/v1/loan/contract/card`,
    method: "put",
    data: {
      card_id: id,
      ord: ord,
    },
  };
  return httpRequest(config);
};

export const getProtocolsByContractId = (contract_id) => {
  let config = {
    url: `${host}/api/v1/loan/contract/protocols/${contract_id}`,
    method: "get",
  };
  return httpRequest(config);
};

// for process page
export const processSearchByDate = (date) => {
  let config = {
    url: `${host}/api/v1/loan/process/date/${date}`,
    method: "get",
  };
  return httpRequest(config);
};

export const processSearchByStatus = (status) => {
  let config = {
    url: `${host}/api/v1/loan/process/status/${status}`,
    method: "get",
  };
  return httpRequest(config);
};

export const processStart = (filial_code) => {
  let config = {
    url: `${host}/api/v1/loan/process/start`,
    method: "put",
    data: {
      filial_code: filial_code,
    },
  };
  return httpRequest(config);
};

export const processStartOld = (filial_code) => {
  let config = {
    url: `${host}/api/v1/loan/process/start-overdue`,
    method: "put",
    data: {
      filial_code: filial_code,
    },
  };
  return httpRequest(config);
};

export const processStop = () => {
  let config = {
    url: `${host}/api/v1/loan/process/stop`,
    method: "put",
  };
  return httpRequest(config);
};

// for params page
export const getParam = () => {
  let config = {
    url: `${host}/api/v1/loan/params`,
    method: "get",
  };
  return httpRequest(config);
};

export const editParamApi = (id, value) => {
  let config = {
    url: `${host}/api/v1/loan/param/id`,
    method: "put",
    data: {
      param_id: id,
      param_value: value,
    },
  };
  return httpRequest(config);
};

export const paramsReloadState = () => {
  let config = {
    url: `${host}/api/v1/loan/reload-states`,
    method: "put",
  };
  return httpRequest(config);
};

export const reco = () => {
  let config = {
    url: `${host}/api/v1/loan/reco`,
    method: "put",
  };
  return httpRequest(config);
};

export const backup = () => {
  let config = {
    url: `${host}/api/v1/loan/backup`,
    method: "put",
  };
  return httpRequest(config);
};

// Schedulers
export const getScheduler = () => {
  let config = {
    url: `${host}/api/v1/loan/schedulers`,
    method: "get",
  };
  return httpRequest(config);
};

export const updateSchedulerAPI = (id, expression) => {
  let config = {
    url: `${host}/api/v1/loan/scheduler`,
    method: "put",
    data: {
      id: id,
      expression: expression,
    },
  };
  return httpRequest(config);
};

export const startSchedulerAPI = (id) => {
  let config = {
    url: `${host}/api/v1/loan/scheduler/start/${id}`,
    method: "put",
  };
  return httpRequest(config);
};

export const stopSchedulerAPI = (id) => {
  let config = {
    url: `${host}/api/v1/loan/scheduler/stop/${id}`,
    method: "put",
  };
  return httpRequest(config);
};

// for transacts page
export const searchTransactByDate = (beginTime, endTime) => {
  let config = {
    url: `${host}/api/v1/loan/transacts`,
    method: "post",
    data: {
      begin_time: beginTime,
      end_time: endTime,
    },
  };
  return httpRequest(config);
};
export const searchTransactByState = (state) => {
  let config = {
    url: `${host}/api/v1/loan/transacts/state/${state}`,
    method: "get",
  };
  return httpRequest(config);
};

export const sendTransactAction = (action, ids) => {
  let config = {
    url: `${host}/api/v1/loan/transacts/action`,
    method: "put",
    data: {
      action: action,
      id: ids,
    },
  };
  return httpRequest(config);
};

export const getTransactByLoanId = (loan_id) => {
  let config = {
    url: `${host}/api/v1/loan/transacts/loan-id/${loan_id}`,
    method: "get",
  };
  return httpRequest(config);
};

export const getProtocols = (transact_id) => {
  let config = {
    url: `${host}/api/v1/loan/transact/protocols/${transact_id}`,
    method: "get",
  };
  return httpRequest(config);
};
// for users page
export const getUser = () => {
  let config = {
    url: `${host}/api/v1/loan/users`,
    method: "post",
    data: {},
  };
  return httpRequest(config);
};

export const addUser = (local_code, login, name, password, role) => {
  let config = {
    url: `${host}/api/v1/loan/user`,
    method: "post",
    data: {
      local_code: local_code,
      login: login,
      name: name,
      password: password,
      role: role,
    },
  };
  return httpRequest(config);
};

export const updateUserState = (user_id, state) => {
  let config = {
    url: `${host}/api/v1/loan/user`,
    method: "put",
    data: {
      user_id: user_id,
      state: state,
    },
  };
  return httpRequest(config);
};

export const updateUserPassword = (user_id, password) => {
  let config = {
    url: `${host}/api/v1/loan/user/pass`,
    method: "put",
    data: {
      user_id: user_id,
      password: password,
    },
  };
  return httpRequest(config);
};
