import axios from "axios";
import { getUser } from "../utilits";

// export const host = "http://localhost:8086";
export const host = process.env.REACT_APP_API_ROOT;

export function httpRequest(config) {
  return axios({
    ...config,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-type": "Application/json",
      Authorization: getUser() ? `Bearer ` + getUser().token : "",
    },
  });
}

//"http://213.230.99.101:2027"
