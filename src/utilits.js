export let setLocalStorage = (key, token) => {
  localStorage.setItem(key, token);
};
export let deleteLocalStorage = (key) => {
  localStorage.removeItem(key);
};

export let getUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};
//"homepage": "https://maxreact.github.io/adminPanel.github.io/",
